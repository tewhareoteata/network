# network

## Network of Te Whare o te Ata

### Introduction

Over the years the network has changed as new technologies have become 
available. Originally copper wires provided a telephone. An ADSL connection was
later implemented over the copper telephone wires to provide internet services. 
The copper wire network is owned by the company that is now called 
[Chorus](https://en.wikipedia.org/wiki/Chorus_Limited)

With the laying of fibre optics throughout Hamilton, the internet and phone 
services at Te Whare o te Ata were changed to using a fibre connection.

Waikato Electricity Limited (WEL) was formed in 1988 from an amalgamation of the 
Central Waikato Electric Power Board with Hamilton City Council's Electricity 
Division. They are now the [WEL Group](https://www.wel.co.nz/about-wel/our-business/), 
incorporating [WEL Networks Limited](https://www.wel.co.nz/about-wel/what-we-do/) 
for providing an electricity network and 
[Ultrafast Fibre Limited](https://www.ultrafastfibre.co.nz/corporate-structure/) 
for providing the fibre optics 
[broadband network](https://en.wikipedia.org/wiki/Ultra-Fast_Broadband).

Ultrafast Fibre Limited work with key partners to grow and manage its UFB network. 
[Broadspectrum Services (New Zealand) Limited](http://www.broadspectrum.com/about-us) 
build and maintain the physical network. 
[Huawei Technologies (New Zealand) Company Limited](https://www.huawei.com/nz/) 
operate the Layer 2 fibre network.

In Summary, the fibre optics was installed to the Te Whare o te Ata building by
Broadspectrum, and the Optical Network Terminal (ONT) box in the office was
provided by Huawei. These services were provided under the umbrella of WEL Group.


### Current ISP Vendor:

In 2019 [Orcon](https://www.orcon.net.nz/) was the ISP. Orcon was foundered in 
New Zealand in 1994. It was first bought out in 2007 and has undergone further 
buy-outs while still preserving its brand name of Orcon. 
[Vocus Group](https://www.vocusgroup.com.au/), an Australian company that is 
listed on the 
[Australian Securities Exchange](https://www.asx.com.au/asx/share-price-research/company/voc), 
is the current owner of the Orcon brand.


### Future ISP Vendor:

Te Whare o te Ata may wish to consider changing to a New Zealand owned ISP.




